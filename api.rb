require 'sinatra'
require 'sinatra/json'
require 'json'

get '/silos' do
  json :name => 'bar'
end

post '/silo/1' do
  json :name => 'bar'
end

put '/silo/1' do
  json :name => 'bar'
end

delete '/silo/1' do
  json :name => 'bar'
end
