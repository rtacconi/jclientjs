# jclientjs - JSON Client JavaScript

## Description

jclientjs is a JSON AJAX client. After incluing the file the function is available as $http. Since the funciton uses JavaScript Promises, you have to use a promise to run a request.

## Installation

Just copy jclient.js and include it in your page:

```html
<script src="your_path/jclient.js"></script>
```

## Usage
var result;

$http('get','/silos').then(function(response) {
  result = response;
}, function(error) {
  console.error("Failed!", error);
});

console.log(result);

$http('post','http://localhost:4567/silo/1', { 'foo': 'bar'}).then(function(response) {
  result = response;
}, function(error) {
  console.error("Failed!", error);
});

console.log(result);

## Project home

https://github.com/rtacconi/jclientjs

## Testing

I have a Ruby API in api.rb that I used to test this client. To run the API you need to install sintra gem. I tried to use Jasmine for testing but for some reason it was not working. Plus, it is very simple to test this function in the browser so there is not any test available.

## Credits

http://www.html5rocks.com/en/tutorials/es6/promises/
